

# LIS 4368

## Lindsey Mabrey

### Assignment #3 Requirements:

*Sub-Heading:*

1. Create a new Git repositroy hosted on bitbucket servers
2. Create and forward-engineer ERD 
3. Answer chapter 7 & 8 questions

#### README.md file should include the following items:

* Bullet-list items
* a3.png ERD
* links for ERD and SQL script




#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD](http://bitbucket.org/lm13d/lis4368a3/raw/master/images/a3.png)

[A3 MWB File](docs/a3.mwb "A3 ERD")

[A3 SQL Script](docs/a3.sql "A3 SQL SCRIPT")