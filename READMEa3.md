> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Lindsey Mabrey

### Assignment #3 Requirements:

*Sub-Heading:*

1. Create a new Git repositroy hosted on bitbucket servers
2. Create and forward-engineer ERD 
3. Answer chapter 7 & 8 questions

#### README.md file should include the following items:

* Bullet-list items
* a3.png ERD

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD](http://bitbucket.org/lm13d/lis4368a3/raw/master/images/a3.png)



readme
readme
readme
readme
